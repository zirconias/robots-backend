package com.imdrissi.rbc.robotsbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobotsBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RobotsBackendApplication.class, args);
	}
}
